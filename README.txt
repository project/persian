
Persian theme 1.0 for phptemplate on Drupal 4.5
----------------------------------------

Last updated: 18 December 2004

This is a port of Persian Theme 1.0 by Chris J. Davis
The original theme can be found at http://www.chrisjdavis.org/persian/

This theme is especially suitable for a personal blog site. It is a fixed-width,
single column design, and Drupal's blocks are hidden as navigation bar.

Customization is relatively easy with CSS. The most obvious one is to replace the header
image (top.png) with your own.

Author
------
Chrisada Sookdhis
Contact: use contact form at http://ichris.ws
