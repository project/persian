<div class="comment <?php print ($comment->new) ? 'comment-new' : '' ?>">
<?php if ($comment->new) : ?>
  <a id="new"></a>
  <span class="new"><?php print $new ?></span>
<?php endif; ?>
  <?php print $picture ?>
  <?php print $content ?>
  <cite>Comment by <?php print $submitted ?></cite>
  <?php if ($picture) : ?>
    <br class="clear" />
  <?php endif; ?>
  <div class="links"><?php print $links ?></div>
</div>
