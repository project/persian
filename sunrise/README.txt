
Sunrise 1.0
A PHPTemplate template theme for Drupal 4.5 by Steve Wright.
Theme home page: http://theme.etmeli.us/
Let me know if you use the theme, and I'll link to it.

This theme is especially suitable for a personal blog site. It is a fixed-width,
single column design, and Drupal's blocks are hidden as navigation bar.

Customization is relatively easy with CSS. The most obvious one is to replace the header
image (top.png) with your own.

----------------------------------------

Release History

23-Jan-2005: Sunrise release, version 1.0

Theme based on Persian Theme 1.0 by Chris J. Davis
Ported to Drupal by Chrisada Sookdhis, use contact form at http://ichris.ws

The original Persian for WordPress theme was done by Chris Davis, and can be found at 
http://www.chrisjdavis.org/persian/

----------------------------------------

