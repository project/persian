<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.1//EN" "http://www.w3.org/TR/xhtml11/DTD/xhtml11.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="<?php print $language ?>">
<head profile="http://gmpg.org/xfn/1">
<title><?php print $head_title ?></title>
<meta http-equiv="Content-Style-Type" content="text/css" />
<?php print $head ?><?php print $styles ?>
<script type="text/javascript" src="themes/persian/drop_down.js"></script>
</head>
<body <?php print theme("onload_attribute"); ?>> 
<div class="container"> 
<div class="header" onclick="location.href='<?php print url() ?>';" style="cursor: pointer;"> 
  <?php if ($site_name) : ?> 
  <div class="sitename"><?php print($site_name) ?> 
    <?php if ($site_slogan) : ?> 
    <br /> 
    <small><?php print($site_slogan) ?></small> 
    <?php endif;?> 
  </div> 
  <?php endif;?> 
</div> 
<div class="wrapper"> 
  <?php if ($sidebar_left != ""): ?> 
  <ul id="topbar"> 
    <?php print $sidebar_left ?> 
    <?php if ($search_box): ?> 
    <li id="search"> 
      <form action="<?php print $search_url ?>" method="post"> 
        <input class="form-text" type="text" size="15" value="search" name="keys" /> 
      </form> 
    </li> 
    <?php endif; ?> 
  </ul> 
  <?php endif; ?> 
  <div class="main"> 
    <?php if ($sidebar_right != ""): ?> 
    <ul id="rightbar"> 
      <?php print $sidebar_right ?> 
    </ul> 
    <?php endif; ?> 
    <div class="main-content" id="content-<?php print $layout ?>"> <?php print $breadcrumb ?> 
      <?php if ($tabs != ""): ?> 
      <?php print $tabs ?> 
      <?php endif; ?>
      <?php if ($page != 0): ?> 
      <?php if ($title != ""): ?> 
      <h2 class="content-title"><?php print $title ?></h2> 
      <?php endif; ?> 
      <?php endif; ?> 
      <?php if ($mission != ""): ?> 
      <div id="mission"><?php print $mission ?></div> 
      <?php endif; ?> 
      <?php if ($help != ""): ?> 
      <p id="help"><?php print $help ?></p> 
      <?php endif; ?> 
      <?php if ($messages != ""): ?> 
      <div id="message"><?php print $messages ?></div> 
      <?php endif; ?> 
      <!-- start main content --> 
      <?php print($content) ?> 
      <!-- end main content --> 
    </div> 
  </div> 
</div> 
<div class="footer"> 
  <?php if ($footer_message) : ?> 
  <?php print $footer_message;?><br /> 
  <?php endif; ?> 
  <a href="http://tinyurl.com/38ez7" title="Cynthia Says Report">Section 508 compliant</a> and <a href="http://tinyurl.com/2jzcv" title="Cynthia Says Report">WAI compliant</a>&nbsp; <a href="http://chrisjdavis.org" title="Chris J. Davis">Design by Chris J. Davis.</a> Powered by <a href="http://drupal.org" title="Drupal">Drupal</a> </div> 
<?php print $closure;?> 
</div>
</body>
</html>
