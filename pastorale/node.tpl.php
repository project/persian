<div class="date">
<strong>
<?php print $date ?>
</strong>
<br />
</div>
<br />

  <div class="title">
<a href="<?php print $node_url ?>" rel="bookmark" title="Permanent Link: <?php print $title ?>"><?php print $title ?></a>
</div>

<div class="bottomsep">
<?php print $terms ?> :: <?php if ($links): ?><?php print $links ?><?php endif; ?>
</div>
  <?php print $content ?>
<br />
<div class="separator"></div>
